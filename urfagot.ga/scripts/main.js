function DisplayLoginWindow (){
	document.getElementsByClassName("darkener")[0].style.visibility="visible";
	document.getElementsByClassName("login-window")[0].style.visibility="visible";
}

function HideLoginWindow (){
	document.getElementsByClassName("darkener")[0].style.visibility="hidden";
	document.getElementsByClassName("login-window")[0].style.visibility="hidden";
}

function initializeGoogleMap () {
	var mapOptions = {
		center: new google.maps.LatLng(59.999009, 30.366057),
		zoom: 18,
		disableDefaultUI: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
	var marker = new google.maps.Marker({position: mapOptions.center, map: map, title: 'Кей'});
	return [map, mapOptions];
};

function initializeMenu (){
	$(document).ready(function() {

		$(".menu").hide();
		$(".navicon").click(function() {
			if ( $(".menu:hidden").length == 1)
			{
				$(".menu").show();
			}
			else
			{
				$(".menu").hide();
			}
		});
	});
}